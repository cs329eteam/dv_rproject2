require(tidyr)
require(dplyr)
require(ggplot2)
require(RCurl)
require(jsonlite)

#The following code is commented out for efficiency.
#It assumes has already run code from 'energy_wrangling.R'
#and that the dataframe new_energy is already created. Uncomment this code to create 
#the dataframe here if necessary.

#----START COMMENTED CODE-----
#energy <- data.frame(fromJSON(getURL(URLencode('oraclerest.cs.utexas.edu:5001/rest/native/?query="select * from energy"'),httpheader=c(DB='jdbc:oracle:thin:@aevum.cs.utexas.edu:1521/f16pdb', USER='cs329e_mas8296', PASS='orcl_mas8296', MODE='native_mode', MODEL='model', returnDimensions = 'False', returnFor = 'JSON'), verbose = TRUE) ))
#summary(energy)

#tbl_df(energy)

##filter out unnecessary columns
#new_energy <- energy %>% select(COUNTRY_OR_AREA, UNIT, YEAR, QUANTITY) %>% tbl_df

#new_energy %>% select(COUNTRY_OR_AREA, UNIT, YEAR, QUANTITY) %>% dplyr::filter(COUNTRY_OR_AREA == "Canada") %>% tbl_df
#-----END COMMENTED CODE-----

#grabs sum of top 10% energy-using countries' usage statistics over time
#converts quantity to numeric
new_energy$QUANTITY<-as.numeric(new_energy$QUANTITY)
#creates new df with countries' energy usage represented as a sum of all of the individual areas
x3 <- new_energy %>% select(COUNTRY_OR_AREA, UNIT, YEAR, QUANTITY) %>% dplyr::group_by(COUNTRY_OR_AREA, UNIT, YEAR) %>% summarise(QUANTITYSUM= sum(QUANTITY))
#grabs data from 2010 for comparisons coming up
test <- x3 %>% select(COUNTRY_OR_AREA,UNIT,QUANTITYSUM,YEAR) %>% dplyr::filter(YEAR==2010)

#uses five large countries to compare total energy use in 2010
x3 <- test %>% dplyr::mutate(quant_percent = cume_dist(QUANTITYSUM)) %>% dplyr::filter(COUNTRY_OR_AREA %in% c("Canada","Japan","China","Russian Federation","India")) %>% dplyr::arrange(desc(QUANTITYSUM))%>% tbl_df
head(x3)



