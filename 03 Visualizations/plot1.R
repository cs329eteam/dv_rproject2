require(ggplot2)

ggplot() + 
  scale_x_continuous() +
  scale_y_continuous() +
  labs(title='Canada') +
  labs(x="Year", y=paste("Killawatt hrs, million")) +
  
  layer(data=x1, 
        mapping=aes(x=as.numeric(as.character(YEAR)), y=as.numeric(as.character(QUANTITY)), color=YEAR),  
        geom="point",
        stat="identity", 
        position=position_jitter(width=0.3, height=0)
        #position=position_identity()
  )
